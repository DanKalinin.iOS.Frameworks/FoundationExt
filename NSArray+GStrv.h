//
//  NSArray+GStrv.h
//  FoundationExt
//
//  Created by Dan Kalinin on 4/12/21.
//

#import "NSEMain.h"

@interface NSArray (GStrv)

- (GStrv)gStrvTo;

@end
