//
//  NSArray+GArray.m
//  FoundationExt
//
//  Created by Dan Kalinin on 12/10/20.
//

#import "NSArray+GArray.h"

@implementation NSArray (GArray)

- (GArray *)gArrayGintTo {
    GArray *ret = g_array_new(FALSE, TRUE, sizeof(gint));
    
    for (NSNumber *number in self) {
        gint sdkNumber = number.intValue;
        ret = g_array_append_val(ret, sdkNumber);
    }
    
    return ret;
}

@end
