//
//  NSError+GError.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/11/20.
//

#import "NSEMain.h"

@interface NSError (GError)

+ (instancetype)gErrorFrom:(GError *)sdkError;

@end
