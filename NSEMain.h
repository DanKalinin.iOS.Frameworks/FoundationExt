//
//  NSEMain.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/11/19.
//

#import <arpa/inet.h>
#import <Foundation/Foundation.h>
#import <GLibExt/GLibExt.h>

#define NSE_BOX(value) \
    ({ \
        (value != NULL) ? @(value) : nil; \
    })
