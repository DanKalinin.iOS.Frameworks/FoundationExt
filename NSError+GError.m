//
//  NSError+GError.m
//  FoundationExt
//
//  Created by Dan Kalinin on 12/11/20.
//

#import "NSError+GError.h"

@implementation NSError (GError)

+ (instancetype)gErrorFrom:(GError *)sdkError {
    gchar *domain = (gchar *)g_quark_to_string(sdkError->domain);
    
    NSMutableDictionary *userInfo = NSMutableDictionary.dictionary;
    userInfo[NSLocalizedDescriptionKey] = NSE_BOX(sdkError->message);
    
    NSError *ret = [self errorWithDomain:NSE_BOX(domain) code:sdkError->code userInfo:userInfo];
    return ret;
}

@end
