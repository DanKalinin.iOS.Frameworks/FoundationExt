//
//  NSArray+GStrv.m
//  FoundationExt
//
//  Created by Dan Kalinin on 4/12/21.
//

#import "NSArray+GStrv.h"

@implementation NSArray (GStrv)

- (GStrv)gStrvTo {
    gchar *ret[self.count + 1];
    
    for (NSUInteger i = 0; i < self.count; i++) {
        NSString *string = self[i];
        ret[i] = (gchar *)string.UTF8String;
    }
    
    ret[self.count] = NULL;
    return g_strdupv(ret);
}

@end
