//
//  NSDate+GDateTime.m
//  FoundationExt
//
//  Created by Dan Kalinin on 12/30/20.
//

#import "NSDate+GDateTime.h"

@implementation NSDate (GDateTime)

+ (instancetype)gDateTimeFrom:(GDateTime *)sdkDateTime {
    gint64 seconds = g_date_time_to_unix(sdkDateTime);
    NSDate *ret = [NSDate dateWithTimeIntervalSince1970:seconds];
    return ret;
}

@end
