//
//  NSEArray.m
//  Intercom
//
//  Created by Dan Kalinin on 1/26/20.
//

#import "NSEArray.h"

@implementation NSArray (NSE)

- (NSString *)nseSQLArray {
    NSString *join = [self componentsJoinedByString:@","];
    NSString *ret = [NSString stringWithFormat:@"(%@)", join];
    return ret;
}

- (NSString *)nseSQLTextArray {
    NSString *join = [self componentsJoinedByString:@"','"];
    NSString *ret = [NSString stringWithFormat:@"('%@')", join];
    return ret;
}

@end

@implementation NSEArray

- (instancetype)initWithStorage:(NSPointerArray *)storage {
    self = super.init;
    
    if (self != nil) {
        _storage = storage;
    }
    
    return self;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    [_storage compact];
    
    for (id anObject in self) {
        if ([anObject respondsToSelector:anInvocation.selector]) {
            [anInvocation invokeWithTarget:anObject];
        }
    }
}

- (NSUInteger)count {
    return _storage.count;
}

- (id)objectAtIndex:(NSUInteger)index {
    void *ret = [_storage pointerAtIndex:index];
    return (__bridge id)ret;
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    [_storage insertPointer:(__bridge void *)anObject atIndex:index];
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    [_storage removePointerAtIndex:index];
}

- (void)addObject:(id)anObject {
    [_storage addPointer:(__bridge void *)anObject];
}

- (void)removeLastObject {
    NSUInteger index = _storage.count - 1;
    [_storage removePointerAtIndex:index];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    [_storage replacePointerAtIndex:index withPointer:(__bridge void *)anObject];
}

@end
