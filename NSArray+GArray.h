//
//  NSArray+GArray.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/10/20.
//

#import "NSEMain.h"

@interface NSArray (GArray)

- (GArray *)gArrayGintTo;

@end
