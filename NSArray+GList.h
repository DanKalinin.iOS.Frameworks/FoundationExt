//
//  NSArray+GList.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/12/20.
//

#import "NSEMain.h"

@interface NSArray (GList)

+ (instancetype)gListGintFrom:(GList *)sdkNumbers;

@end
