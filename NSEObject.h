//
//  NSEObject.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/11/19.
//  Copyright © 2019 Dan Kalinin. All rights reserved.
//

#import "NSEMain.h"

@protocol NSEObject;










@interface NSEObject : NSObject

@end










@protocol NSEObject <NSObject>

@end
