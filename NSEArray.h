//
//  NSEArray.h
//  Intercom
//
//  Created by Dan Kalinin on 1/26/20.
//

#import "NSEMain.h"

@interface NSArray (NSE)

@property (readonly) NSString *nseSQLArray;
@property (readonly) NSString *nseSQLTextArray;

@end

@interface NSEArray : NSMutableArray

@property (readonly) NSPointerArray *storage;

- (instancetype)initWithStorage:(NSPointerArray *)storage;

@end
