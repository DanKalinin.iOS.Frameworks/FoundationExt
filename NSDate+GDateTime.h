//
//  NSDate+GDateTime.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/30/20.
//

#import "NSEMain.h"

@interface NSDate (GDateTime)

+ (instancetype)gDateTimeFrom:(GDateTime *)sdkDateTime;

@end
