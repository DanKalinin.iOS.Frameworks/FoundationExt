//
//  NSArray+GList.m
//  FoundationExt
//
//  Created by Dan Kalinin on 12/12/20.
//

#import "NSArray+GList.h"

@implementation NSArray (GList)

+ (instancetype)gListGintFrom:(GList *)sdkNumbers {
    NSMutableArray<NSNumber *> *ret = NSMutableArray.array;

    for (GList *sdkNumber = sdkNumbers; sdkNumber != NULL; sdkNumber = sdkNumber->next) {
        NSNumber *number = @(GPOINTER_TO_INT(sdkNumber->data));
        [ret addObject:number];
    }

    return ret;
}

@end
